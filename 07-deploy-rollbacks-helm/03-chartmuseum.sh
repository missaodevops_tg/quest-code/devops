#!/bin/sh
SCRIPTPATH=$(cd `dirname -- $0` && pwd)

# Install
helm install --name-template helm --namespace devops -f $SCRIPTPATH/02-chartmuseum-conf.yaml stable/chartmuseum

helm plugin install https://github.com/chartmuseum/helm-push

sleep 60

# Add to repository
helm repo add questcode http://$(kubectl get nodes --namespace devops -o jsonpath="{.items[0].status.addresses[0].address}"):30010

helm lint $SCRIPTPATH/01-charts/backend-scm/
helm push $SCRIPTPATH/01-charts/backend-scm/ questcode

helm lint $SCRIPTPATH/01-charts/backend-user/
helm push $SCRIPTPATH/01-charts/backend-user/ questcode

helm lint $SCRIPTPATH/01-charts/frontend/
helm push $SCRIPTPATH/01-charts/frontend/ questcode

helm repo update

# Deploy
helm install questcode/backend-user --name-template backend-user --namespace staging --set image.tag=alpha \
    && helm install questcode/backend-scm --name-template backend-scm --namespace staging --set image.tag=alpha \
    && helm install questcode/frontend --name-template frontend --namespace staging --set image.tag=alpha
