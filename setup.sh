#!/bin/sh
SCRIPTPATH=$(cd `dirname -- $0` && pwd)

# Limpando o que ja existe
yes | sudo kubeadm reset \
    && sudo systemctl stop docker && sudo systemctl stop kubelet \
    && sudo rm -rf /etc/kubernetes/ \
    && sudo rm -rf .kube/ \
    && sudo rm -rf /var/lib/kubelet/ \
    && sudo rm -rf /var/lib/cni/ \
    && sudo rm -rf /etc/cni/ \
    && sudo rm -rf /var/lib/etcd/ \
    && sudo iptables -F && sudo iptables -t nat -F && sudo iptables -t mangle -F && sudo iptables -X \
    && sudo systemctl start docker && sudo systemctl start kubelet

#Rodando
# Não é possível especificar na configuração o swapoff pois ele da confito
sudo kubeadm init --pod-network-cidr=10.244.0.0/16 --ignore-preflight-errors=Swap

mkdir -p $HOME/.kube \
      && sudo cp /etc/kubernetes/admin.conf $HOME/.kube/config \
	  && sudo chown $(id -u):$(id -g) $HOME/.kube/config

# Taunt
kubectl taint nodes --all node-role.kubernetes.io/master- \
    && kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

#Name spaces do questcode
kubectl apply -f $SCRIPTPATH/06-questcode-kubernetes/01-namespaces.yaml \
    && kubectl apply -f $SCRIPTPATH/06-questcode-kubernetes/02-configmap.yaml \
    && kubectl apply -f $SCRIPTPATH/06-questcode-kubernetes/02-secrets.yaml

echo "Deu bom irmão"
echo $SCRIPTPATH/07-deploy-rollbacks-helm/03-chartmuseum.sh
echo $SCRIPTPATH/08-pipeline-ci-cd-jenkins/01-jenkins-setup/01-jenkins-setup.sh
