#!/bin/sh
SCRIPTPATH=$(cd `dirname -- $0` && pwd)

# Install 
kubectl apply -f $SCRIPTPATH/01-jenkins-pv-pvc.yaml

helm install --name-template jenkins --set persistence.existingClaim=jenkins --set master.serviceType=NodePort --set master.nodePort=30808 --namespace devops stable/jenkins --values $SCRIPTPATH/01-jenkins-values.yaml


# Create authorization 
kubectl create rolebinding sa-devops-role-clusteradmin \
    --clusterrole=cluster-admin\
    --serviceaccount=devops:default --namespace=devops

kubectl create rolebinding sa-devops-role-clusteradmin-kubesystem \
    --clusterrole=cluster-admin \
    --serviceaccount=devops:default --namespace=kube-system

kubectl create clusterrolebinding serviceaccounts-cluster-admin \
  --clusterrole=cluster-admin \
  --group=system:serviceaccounts:devops

# Show Info
NODE_PORT=$(kubectl get --namespace devops -o jsonpath="{.spec.ports[0].nodePort}" services jenkins)
NODE_IP=$(kubectl get nodes --namespace devops -o jsonpath="{.items[0].status.addresses[0].address}")

echo Jenkins
echo URL: http://$NODE_IP:$NODE_PORT/login
echo User: admin
echo Password: $(kubectl get secret --namespace devops jenkins -o jsonpath="{.data.jenkins-admin-password}" | base64 --decode)